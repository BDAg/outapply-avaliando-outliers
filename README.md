# OutApply - Avaliando Outliers

Projeto desenvolvido para a disciplina de projeto integrador da Fatec Shunji Nishimura em Pompeia-SP, que busca desenvolver uma aplicação web para a
 análise de pontos discrepantes.
 
 # Técnicas abordadas

  

- Utilização de estatística descritiva e testes estatísticos.
 
- Manipulação de dados.

- Aplicação web "Shiny APP"




### Ferramentas e Tecnologias utilizadas


 [R](https://www.r-project.org/)

 [Rstudio](https://rstudio.com/)

 [Shiny](https://shiny.rstudio.com/)

 [Pacote outliers](https://cran.r-project.org/web/packages/outliers/outliers.pdf)

 [Pacote EnvStats](https://cran.r-project.org/web/packages/EnvStats/EnvStats.pdf)

 [MS Office](https://www.office.com/)


### Desenvolvedores do projeto


Equipe formada por alunos do curso de Big Data no Agronegócio e seus perfis no LinkedIn.



| Equipe | Perfil no LinkedIn  |
| ------ | ------ |
| Gabriel Dias de Oliveira De Marchi | https://www.linkedin.com/in/gabriel-dias-de-oliveira-de-marchi-412230129/ |
| Juvenal Nobuhiro Yoshikawa | https://www.linkedin.com/in/juvenal-nobuhiro-yoshikawa-94191bb5/ |
| José Eduardo Gonçalves Maran | https://www.linkedin.com/in/gabriel-dias-de-oliveira-de-marchi-412230129/ |
| Nathan da Silva Santos | https://www.linkedin.com/in/gabriel-dias-de-oliveira-de-marchi-412230129/ |


Para mais informações do projeto: [wiki](https://gitlab.com/BDAg/outapply-avaliando-outliers/-/wikis/home)





